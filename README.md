Usage example:

```
Exec {
  path => ['/sbin', '/bin', '/usr/sbin', '/usr/bin', '/usr/local/bin']
}

stage { 'setup':
  before => Stage['main']
}

class { 'webdev::setup':
  stage => 'setup',
}



class { 'webdev::php::engine': }

webdev::php::pool { 'my_pool':
  require => Class['webdev::php::engine'],
}



class { 'webdev::nginx::server': }

webdev::nginx::host { 'my_server.dev':
  require => Class[webdev::nginx::server],
}



class { 'webdev::mysql::server':
  root_password => '1234',
}

webdev::mysql::db { 'my_db':
  root_password => '1234',
}

webdev::mysql::user { 'adminuser':
  password => 'adminpass',
  database => 'my_db',
  privileges => 'CREATE,SELECT,INSERT,DELETE,UPDATE',
  root_password => '1234',
}

webdev::mysql::query { 'setup.sql':
  user => 'adminuser',
  password => 'adminpass',
  database => 'my_db',
}
```

define webdev::mysql::db(
  $database       = $title,
  $character_set  = 'latin1',
  $collate        = 'latin1_general_ci',
  $root_password,
)
{
  exec { "create db $database":
    command => "echo CREATE DATABASE IF NOT EXISTS $database CHARACTER SET $character_set COLLATE $collate | mysql -u root -p$root_password",
    require => Class['webdev::mysql::server'],
  }
}

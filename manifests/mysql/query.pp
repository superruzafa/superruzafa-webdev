define webdev::mysql::query(
  $source       = $title,
  $user,
  $password,
  $database
)
{
  exec { "Run MySQL queries from $title":
    command => "mysql -u $user -p$password $database < $source",
    cwd => dirname($source), # requires stdlib
    require => [
      Class['webdev::mysql::server'],
      Webdev::Mysql::Db[$database],
      Webdev::Mysql::User[$user],
    ],
  }
}

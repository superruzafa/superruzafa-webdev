class webdev::mysql::server(
  $root_password,
)
{
  package { 'mysql-server':
    ensure => present,
  }

  service { 'mysql':
    ensure => running,
    enable => true,
    require => Package['mysql-server'],
  }

  exec { 'set root password':
    command => "mysqladmin -u root password $root_password",
    unless => "mysqladmin -u root -p$root_password status",
    require => Service['mysql'],
  }
}

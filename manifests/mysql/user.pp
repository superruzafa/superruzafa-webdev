define webdev::mysql::user(
  $user       = $title,
  $database,
  $password,
  $host       = 'localhost',
  $privileges = 'SELECT',
  $root_password,
)
{
  exec { "grant privileges to user $user@$host":
    command => "echo GRANT $privileges ON $database.* TO $user@$host IDENTIFIED BY '\"'$password'\"' | mysql -u root -p$root_password",
    require => Webdev::Mysql::Db[$database],
  }
}

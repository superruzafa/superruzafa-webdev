define webdev::nginx::host(
  $servername = $title,
  $source,
) {
  $available_conf = "/etc/nginx/sites-available/$servername"
  $enabled_conf = "/etc/nginx/sites-enabled/$servername"

  file { $available_conf:
    ensure => file,
    source => $source,
  }

  file { $enabled_conf:
    ensure => link,
    target => $available_conf,
    require => File[$available_conf],
    notify => Class['webdev::nginx::service'],
  }
}

class webdev::nginx::server
{
  package { 'nginx':
    ensure => present,
    require => Package['php5-fpm']
  }

  file { ['/etc/nginx/sites-available/default', '/etc/nginx/sites-enabled/default']:
    ensure => absent,
    require => Package['nginx'],
  }

  include nginx::service
}

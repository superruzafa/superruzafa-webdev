class webdev::nginx::service
{
  service { 'nginx':
    ensure => running,
    enable => true,
  }
}

class webdev::php::engine(
  $pid                  = '/var/run/php5-fpm.pid',
  $error_log            = '/var/log/php5-fpm.log',

  $install_fpm          = true,
  $install_cli          = true,
  $install_mysql        = false,
)
{
  if $install_fpm {
    package { 'php5-fpm':
      ensure => present,
    }

    file { '/etc/php5/fpm/php-fpm.conf':
      ensure => file,
      content => template('webdev/php/php-fpm.conf.erb'),
      require => Package['php5-fpm'],
    }

    include php::service
  }

  if $install_cli {
    package { 'php5-cli':
      ensure => present,
    }
  }

  if $install_mysql {
    package { 'php5-mysql':
      ensure => present,
    }
  }

}
